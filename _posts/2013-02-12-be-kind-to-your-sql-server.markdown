---
layout: post
image: burning-server.jpg
thumb: computer-fire.png
---

Is your SQL server silently and needlessly suffering? Sure it runs fine when you test it against local data but what about scalability? Here are some tips that if you implement early you can avoid rewrites later when your data set grows large.

<!--more-->
<BR CLEAR="ALL" />
## Don't be so chatty

If you don't read the rest of this post at least make sure you aren't making this mistake. SQL servers shouldn't be treated like an in memory array that you just request the key when you want. PHP has to to a lot of work (compared to array access) to query the database. The database has to work to get the data and you also have latency issues to transport the data between the apache server and the mysql server especially since most sql servers are not on the same machine. Assume each example has an array full of user ids.

Here is an example of what **not** to do.

	$usernames = [];
	foreach ($userids as $userid) {
		// This loop might generate hundereds or thousands of database queries
		$usernames[$userid] = "SELECT username FROM usertable WHERE id = {$userid}";
	}
And here is the alternative 

	$useridscsv = implode("','");
	$usernames = [];
	foreach ("SELECT username FROM usertable WHERE id IN ('{$useridscsv}')" as $record) {
		$usernames[$id] = $record->username;
	}

Essentially it boils down to **Don't use queries in loops!** There may be exceptions to this but they are probably much less than you think.

## Request only the data you need

At WebCourseworks, where I work, we have some table models that are rather large and they tend to grow larger over time. You should also plan for your table structure to change over time. If you are printing a list of users in a table with username, first name, last name, and deleted status don't query the database with `SELECT * FROM usertable`. You should instead request just the fields you need `SELECT id, username, firstname, lastname FROM usertable`. 

## Get on top of JOIN ON

When you join tables with just a comma and throw all the conditions in the WHERE condition, mysql has to do a lot more work at creating mappings between the two tables. Be explicit and use ON.

## Don't make the database repeat itself

I know it is so easy to have the database stitch the data together for you with a join but with a little more PHP you can potentially avoid a lot of data transfer. For this example we'll assume that we have a usertable a tagtable and a link between the two with a usertagtable. Lets say we wanted to print a list of all user's with their corresponding tags. One simple way to do it might be.
	$userrecords = "SELECT u.id, u.otherstuff, GROUP_CONCAT(t.tagname) FROM usertable u LEFT JOIN usertagtable ut ON ut.userid = u.id JOIN tagtable t ON t.id = ut.tagid GROUP BY u.id";

Nice and simple, unfortunately there is a way to do this that is much more efficient but a little more work. The problem with the above method is that it is sending back the same tag names over and over again since many users share the same tags. Imagine if the site allowed really long tag names and you had tens of thousands of users that need to go into this report. Instead you should return only the tag ids for the user and have a separate query for the tag names which you can use PHP to translate.

	$tagnames = "SELECT id,tagname FROM tagtable";
	$userrecords = "SELECT u.id, u.otherstuf, GROUP_CONCAT(ut.id) as tagids FROM usertable u LEFT JOIN tagtable ut ON ut.userid = u.id";
	foreach ($userrecords as $userrecord) {
		$userrecord->tags = some_function_to_explode_tag_ids_and_return_the_imploded_names($userrecord->tagsids, $tagnames);
	}


## Avoid unions

In a report that I developed we had a 3 table union that would select a sub set of user's records for various types of training. Learn from my mistake and don't use them if there is a way around it. A better alternative to the above approach would be to pull out a recordset for each table and stitch it together in PHP. Take a look at my [multi pdo itterator](http://bitbucket.org/matthewdaniel/multipdostatementiterator). This can help you avoid unions and also it might be helpful when joining all the way to something makes for very ugly sql.

matt.meisberger