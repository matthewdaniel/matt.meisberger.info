---
layout: post
image: clean-desk.jpg
thumb: clean-desk.jpg
---

Using PHP's some of lesser used functions can help declutter code and increase code readability. I've been experimenting a lot lately with working array\_walk, array\_map, array\_filter, and array\_reduce into my code and have found some benefits to doing so. Read on to see what I've learned from using these.

<!--more-->

<br />

## Helps debugging
Unless you have a form of function-itise, it is hard to always avoid loops with a fair bit of code in them. When looking over old code and coming across these loops while looking for a bug you have to slow way down and inspect all of the loop to see if it is modifying any of your state variables or passed in parameters. The map reduce type functions can greatly help when it comes to this because you have to explicitly tell it which variables to use and which variables that are going to be modified. If you are looking for a bug trying to trace out a variable path you can safely skip over an entire section of code if it is either not used or passed in statically. 

## Increases readability
Having one loop that builds out an array of data that does multiple things is not the easiest thing to read. For example

	$coursequizzes = "SELECT id,score FROM user_quizes WHERE courseid = ?";
	$allscores = [];
	$bestscore = false;
	$worstscore = false;
	foreach ($coursequizzes as $record) {
		if ($record->userid != $userid)
			continue;
		}

		if ($worstscore === false || $record->score < $worstscore) {
			$worstscore = $record->score;
		}

		if ($bestscore === false || $record->score > $bestscore) {
			$bestscore = $record->score;
		}

		$allscores[] = $record->score;
	}
	$average = get_average_from_all_scores($allscores);

	echo '<html><tbody>';
	foreach ($userquizzes as $quiz) {
		// add a bunch of html that prints out a table that 
		// relies on the variables we just fetched
		// which could be fairly long if you need conditional 
		// classes or conditional columns
	}
	echo '</tbody></html>';

--

	$coursequizzes = "SELECT id,score FROM user_quizes WHERE courseid = ?";

	$userquizzes = array_filter($coursequizzes, function ($record) use($userid) {
		return $record->userid == $userid;
	});

	$allscores = array_map(function ($record) {
		return $record->score;
	}, $userquizzes);
	$average = get_average_from_all_scores($allscores);

	$bestscore = array_reduce(function ($bestscore, $record) {
		return empty($bestscore) || $record->score > $bestscore ? $record->bestscore : $bestscore;
	}, $userquizzes);

	$worstscore = array_reduce(function ($worstscore, $record) {
		return empty($worstscore) || $record->score < $worstscore ? $record->worstscore : $worstscore;
	}, $userquizzes);

	echo '<html><tbody>';
	array_walk($userquizzes, function ($quiz) use($bestscore, worstscore, $average) {
		// add a bunch of html that prints out a table that 
		// relies on the variables we just fetched
		// which could be fairly long if you need conditional 
		// classes or conditional columns
	});
	echo '</tbody></html>';
In the second example we have clearly defined units of work for each of our variables. You know exactly what each section of code does simply by the variable name. You don't have to peice together which part of the function is for which state variables. We also know that no matter how large are array_walk becomes (or any of them for that matter) that it isn't changing any variables because it has its own scope. 

Note: Use common sense with these. If you are iterating over huge amounts of data then likely you may want to just loop once even if it is less readable.

## Frustrations
Stacking them seems to get pretty ugly if the outside one doesn't have enough code unlike a regular loop. Most of all the worst thing is that the function parameters don't match

<table class="table">
	<thead>
		<tr>
			<th>Function name</th>
			<th>First Param</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>array_filter</td>
			<td>array</td>
		</tr>
		<tr>
			<td>array_walk</td>
			<td>array</td>
		</tr>
		<tr>
			<td>array_reduce</td>
			<td>array</td>
		</tr>
		<tr>
			<td>array_map</td>
			<td>callback</td>
		</tr>
	</tbody>
</table>
