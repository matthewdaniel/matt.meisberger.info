---
layout: post
image: jekyllvswordpress.png
thumb: jekyll.png
---

I have wanted to set up a website which I can use to blog and keep my resume update. 
Working 50-60+ hours a week has left me with little time to actually do anything about it. 
Isn't this why WordPress exists? Just download their software and unzip it on a LAMP server, choose
a theme and everything is up and running.   

<!--more-->

<BR CLEAR="ALL" />

## Why not WordPress?
WordPress is a slick product. Very intuitive; easy setup; lots of themes; huge community plugins; What's not to love, right?
Well there are downsides to using WordPress. I actually had in installed and my site ready to start posting things to but I 
couldn't bring my self to use it. Here are a few of the reasons why I couldn't do it.

#### Vulnerability
Leaving my server as vulnerable as a car without windows is about as appealing to me as the next guy (just as Dale Sande from anotheruiguy.com who's blog is hacked at the time of writing).
WordPress is notorious for having zero day vulnerabilities, some of which allows rogue php code to be run on the server.
My server is set up to run multiple client Moodle installations and this is just not an acceptable risk. But wait, there's more.

#### Efficiency and Speed
Dynamically generating HTML every single request is very inefficient. The content never changes and there is no need to spin up the
PHP compiler to keep generating the same thing over and over again. It is similar to disassembling and re-assembling your bike every
time you start and stop a bike ride. The parts don't unexpectedly change how they piece together so why not just leave it assembled 
to save time and effort. For a server, serving up a static HTML page is effortless, serving up a PHP request requires it to create a worker
for the request, reserve memory for a while after the request is served (TTL), compiling the PHP script into machine code, request records
from the database, compile the results into HTML and then effortlessly serve up that HTML page.

## Alternatives?
One alternative is to open up notepad like the it was the 90's and just start typing all the html. No PHP to dynamically load 
in reusable parts of HTML like the header, footer, and sideblocks. No PHP to loop through blog posts and paginate posts and 
dynamically generate the links. Just straight HTML coding? Yuk. Fortunately there is a middle ground. Meet static site generators.

#### Static site generators
These things do the PHP like compiling on the local machine and generate the good old fashion HTML website which can then in turn be
uploaded to a server that only serves static files. It is the best of both worlds. No per request compiling, no apache or mysql to 
worry about, and free hosting on Amazon servers. The static site generator I choose was Jekyll. This is not because of any 
perceived superiority but rather the attractiveness of a large community. Also there is probably a good reason Github choose it so
why spend the time pouring into research. After all I'm busy.

## Experience
With existing content and an existing theme it would probably take just about 3 hours to convert from WordPress **and learn Jekyll**
assuming that you are a little familiar with basic programming concepts such as loops and conditionals. Maybe one of these days I'll 
write a tutorial on converting a WordPress site.

matt.meisberger